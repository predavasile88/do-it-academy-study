﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmTest : Form
    {
        private string nextPlayer;

        public frmTest()
        {
            InitializeComponent();
            nextPlayer = "X";
        }

        private void frmTest_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    int b = 0;
            //    throw new Exception("exceptia mea");
            //    //int a = 10 / b;
            //    MessageBox.Show("sdfsfasd");

            //}
            //catch(DivideByZeroException ex)
            //{
            //    MessageBox.Show("exceptie");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("a aparut o eroare: " + ex.Message);
            //}

            //List<int> lst = new List<int>();
            //lst.Add(5);
            //lst.Add(1);
            //lst.Add(10);
            //lst.Add(8);
            //lst.Add(1);

            //int max = 0;
            //int i;
            //for (i = 0; i < lst.Count; i++)
            //{
            //    if (max < lst[i])
            //    {
            //        max = lst[i];
            //    }
            //}

            //MessageBox.Show("maximul este: " + max);

        }

        private void btn_Click(object sender, EventArgs e)
        {
            ((Button)sender).Text = nextPlayer;
            ((Button)sender).Enabled = false;

            if (isWinner())
            {
                MessageBox.Show("A castigat " + nextPlayer);
            }

            if (nextPlayer == "X")
            {
                nextPlayer = "0";
            }
            else
            {
                nextPlayer = "X";
            }
        }

        private bool isWinner()
        {
            if ((btn1.Text == nextPlayer && btn2.Text == nextPlayer && btn3.Text == nextPlayer) ||
                (btn4.Text == nextPlayer && btn5.Text == nextPlayer && btn6.Text == nextPlayer) ||
                (btn7.Text == nextPlayer && btn8.Text == nextPlayer && btn9.Text == nextPlayer) ||
                (btn1.Text == nextPlayer && btn4.Text == nextPlayer && btn7.Text == nextPlayer) ||
                (btn2.Text == nextPlayer && btn5.Text == nextPlayer && btn8.Text == nextPlayer) ||
                (btn3.Text == nextPlayer && btn6.Text == nextPlayer && btn9.Text == nextPlayer) ||
                (btn1.Text == nextPlayer && btn5.Text == nextPlayer && btn9.Text == nextPlayer) ||
                (btn3.Text == nextPlayer && btn5.Text == nextPlayer && btn7.Text == nextPlayer))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
