﻿namespace WindowsFormsApp1
{
    partial class frmEditCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.pbImagine = new System.Windows.Forms.PictureBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.txtImagine = new System.Windows.Forms.TextBox();
            this.lblImage = new System.Windows.Forms.Label();
            this.lblIsNew = new System.Windows.Forms.Label();
            this.rbNu = new System.Windows.Forms.RadioButton();
            this.rbDa = new System.Windows.Forms.RadioButton();
            this.txtNume = new System.Windows.Forms.TextBox();
            this.lblNume = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagine)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(383, 406);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "ANULARE";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(99, 406);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(156, 23);
            this.btnEdit.TabIndex = 20;
            this.btnEdit.Text = "SALVARE CATEGORIE";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // pbImagine
            // 
            this.pbImagine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbImagine.Location = new System.Drawing.Point(99, 145);
            this.pbImagine.Name = "pbImagine";
            this.pbImagine.Size = new System.Drawing.Size(359, 231);
            this.pbImagine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagine.TabIndex = 19;
            this.pbImagine.TabStop = false;
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(383, 106);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 18;
            this.btnPreview.Text = "PREVIEW";
            this.btnPreview.UseVisualStyleBackColor = true;
            // 
            // txtImagine
            // 
            this.txtImagine.Location = new System.Drawing.Point(102, 108);
            this.txtImagine.Name = "txtImagine";
            this.txtImagine.Size = new System.Drawing.Size(269, 20);
            this.txtImagine.TabIndex = 17;
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Location = new System.Drawing.Point(8, 108);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(47, 13);
            this.lblImage.TabIndex = 16;
            this.lblImage.Text = "Imagine:";
            // 
            // lblIsNew
            // 
            this.lblIsNew.AutoSize = true;
            this.lblIsNew.Location = new System.Drawing.Point(5, 69);
            this.lblIsNew.Name = "lblIsNew";
            this.lblIsNew.Size = new System.Drawing.Size(84, 13);
            this.lblIsNew.TabIndex = 15;
            this.lblIsNew.Text = "Categorie Noua:";
            // 
            // rbNu
            // 
            this.rbNu.AutoSize = true;
            this.rbNu.Location = new System.Drawing.Point(158, 67);
            this.rbNu.Name = "rbNu";
            this.rbNu.Size = new System.Drawing.Size(41, 17);
            this.rbNu.TabIndex = 14;
            this.rbNu.TabStop = true;
            this.rbNu.Text = "NU";
            this.rbNu.UseVisualStyleBackColor = true;
            // 
            // rbDa
            // 
            this.rbDa.AutoSize = true;
            this.rbDa.Location = new System.Drawing.Point(102, 67);
            this.rbDa.Name = "rbDa";
            this.rbDa.Size = new System.Drawing.Size(40, 17);
            this.rbDa.TabIndex = 13;
            this.rbDa.TabStop = true;
            this.rbDa.Text = "DA";
            this.rbDa.UseVisualStyleBackColor = true;
            // 
            // txtNume
            // 
            this.txtNume.Location = new System.Drawing.Point(102, 22);
            this.txtNume.Name = "txtNume";
            this.txtNume.Size = new System.Drawing.Size(269, 20);
            this.txtNume.TabIndex = 12;
            // 
            // lblNume
            // 
            this.lblNume.AutoSize = true;
            this.lblNume.Location = new System.Drawing.Point(5, 25);
            this.lblNume.Name = "lblNume";
            this.lblNume.Size = new System.Drawing.Size(38, 13);
            this.lblNume.TabIndex = 11;
            this.lblNume.Text = "Nume:";
            // 
            // frmEditCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.pbImagine);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.txtImagine);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lblIsNew);
            this.Controls.Add(this.rbNu);
            this.Controls.Add(this.rbDa);
            this.Controls.Add(this.txtNume);
            this.Controls.Add(this.lblNume);
            this.Name = "frmEditCategory";
            this.Text = "frmEditCategory";
            this.Load += new System.EventHandler(this.frmEditCategory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.PictureBox pbImagine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.TextBox txtImagine;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label lblIsNew;
        private System.Windows.Forms.RadioButton rbNu;
        private System.Windows.Forms.RadioButton rbDa;
        private System.Windows.Forms.TextBox txtNume;
        private System.Windows.Forms.Label lblNume;
    }
}