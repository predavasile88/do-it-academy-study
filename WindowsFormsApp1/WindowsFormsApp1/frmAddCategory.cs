﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmAddCategory : Form
    {
        public Category NewCategory { get; set; }

        public frmAddCategory()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.png)|*.jpg;*.jpeg;*.png";

            DialogResult rezultat = dialog.ShowDialog();
            if (rezultat == DialogResult.OK)
            {
                this.txtImagine.Text = dialog.FileName;
                this.pbImagine.Image = Image.FromFile(dialog.FileName);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtNume.Text) || string.IsNullOrEmpty(this.txtImagine.Text))
            {
                MessageBox.Show("Eroare validare date!");
            }
            else
            {
                this.NewCategory = new Category();
                this.NewCategory.Name = this.txtNume.Text;
                this.NewCategory.IsNew = this.rbDa.Checked;
                this.NewCategory.Image = this.txtImagine.Text;
                this.NewCategory.Id = 1;

                this.Close();
            }
          
        }
    }
}
