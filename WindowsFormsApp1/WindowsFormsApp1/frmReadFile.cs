﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public partial class frmReadFile : Form
    {
        private string _fileName;

        public frmReadFile()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Selectati un fisier";
            dialog.Filter = "Only text files(.txt)|*.txt";

            DialogResult rezultat = dialog.ShowDialog();
            if (rezultat == DialogResult.OK)
            {
                this.richFile.Text = "";
                this._fileName = dialog.FileName;
                StreamReader reader = new StreamReader(dialog.FileName);

                string line = reader.ReadLine();

                while (line != null)
                {
                    this.richFile.Text += line;
                    line = reader.ReadLine();

                    if (line != null)
                    {
                        this.richFile.Text += Environment.NewLine;
                    }
                }

                reader.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            StreamWriter writer = new StreamWriter(this._fileName);
            writer.Write(this.richFile.Text);
            writer.Close();
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Selectati un fisier";
            dialog.Filter = "Only text files(.txt)|*.txt";

            DialogResult rezultat = dialog.ShowDialog();
            if (rezultat == DialogResult.OK)
            {
                this._fileName = dialog.FileName;

                StreamWriter writer = new StreamWriter(dialog.FileName);
                writer.Write(this.richFile.Text);
                writer.Close();
            }
        }

        private void btnSerialize_Click(object sender, EventArgs e)
        {
            Category cat = new Category();
            cat.Id = 1;
            cat.Name = "categoria mea";
            cat.IsNew = true;
            cat.Image = "imaginea categoriei";

            XmlSerializer serializer = new XmlSerializer(typeof(Category));
            StreamWriter writer = new StreamWriter(@"D:\cat.xml");
            serializer.Serialize(writer, cat);
            writer.Close();
        }
    }
}
