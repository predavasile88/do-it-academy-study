﻿using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public bool IsNew { get; set; }
    }
}
