﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmEditCategory : Form
    {
        public Category ModifiedCategory { get; set; }

        public frmEditCategory(Category categ)
        {
            InitializeComponent();

            this.ModifiedCategory = categ;
        }

        private void frmEditCategory_Load(object sender, EventArgs e)
        {
            this.txtNume.Text = this.ModifiedCategory.Name;
            this.rbDa.Checked = this.ModifiedCategory.IsNew;
            this.rbNu.Checked = !this.ModifiedCategory.IsNew;
            this.txtImagine.Text = this.ModifiedCategory.Image;

            if (!String.IsNullOrEmpty(this.ModifiedCategory.Image) && 
                File.Exists(this.ModifiedCategory.Image))
            {
                this.pbImagine.Image = Image.FromFile(this.ModifiedCategory.Image);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.ModifiedCategory.Name = this.txtNume.Text;
            this.ModifiedCategory.IsNew = this.rbDa.Checked;
            this.ModifiedCategory.Image = this.txtImagine.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
