﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private Shop coffeeShop;

        public Form1()
        {
            InitializeComponent();

            this.coffeeShop = new Shop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this.cboTest.SelectedIndex.ToString());
            //MessageBox.Show(this.cboTest.SelectedItem.ToString());
            //MessageBox.Show(this.cboTest.SelectedText);

            //this.rbOptiune2.Checked = true;

            //if (this.rbOptiune1.Checked == true)
            //{
            //    MessageBox.Show("optiunea 1 a fost selectata!");
            //}
            //else if(this.rbOptiune2.Checked == true)
            //{
            //    MessageBox.Show("optiunea 2 a fost selectata!");
            //}
            //else
            //{
            //    MessageBox.Show("nicio optiune nu a fost selectata!");
            //}


        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            frmAddCategory frm = new frmAddCategory();
            frm.ShowDialog();

            if (frm.NewCategory != null)
            {


                this.coffeeShop.Categories.Add(frm.NewCategory);
             //   this.txtNumeCategorie.Text = this.coffeeShop.Categories.Count.ToString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.coffeeShop.Categories.Count; i++)
            {
                this.addCategoryInDGV(this.coffeeShop.Categories[i]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult rezultat =  MessageBox.Show("Sunteti sigur?", "Confirma", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (rezultat == DialogResult.Yes)
            {
                MessageBox.Show("ati selectat yes!");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            frmEditCategory frm = new frmEditCategory(this.coffeeShop.Categories[0]);
            DialogResult result = frm.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.coffeeShop.Categories[0] = frm.ModifiedCategory;
            }
        }

        private void tsItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsItemAddCategory_Click(object sender, EventArgs e)
        {
            frmAddCategory frm = new frmAddCategory();
            frm.ShowDialog();

            if (frm.NewCategory != null)
            {
                int max = 0;
                for (int i = 0; i < this.coffeeShop.Categories.Count; i++)
                {
                    if (max < this.coffeeShop.Categories[i].Id)
                    {
                        max = this.coffeeShop.Categories[i].Id;
                    }
                }
                frm.NewCategory.Id = max + 1;
                this.coffeeShop.Categories.Add(frm.NewCategory);

                this.addCategoryInDGV(frm.NewCategory);
            }
        }



        private void tsItemAbout_Click(object sender, EventArgs e)
        {
            //frmAbout frm = new frmAbout();
            //frm.ShowDialog();
        }

        private void addCategoryInDGV(Category cat)
        {
            int index = dgvCategories.Rows.Add();
            DataGridViewRow row = dgvCategories.Rows[index];
            row.Cells["colNumeCategorie"].Value = cat.Name;
            if (cat.IsNew)
            {
                row.Cells["colIsNewCategorie"].Value = "DA";
            }
            else
            {
                row.Cells["colIsNewCategorie"].Value = "NU";
            }
            row.Cells["colDetaliiCategorie"].Value = "...";
            row.Cells["colEditCategorie"].Value = "EDIT";
            row.Cells["colDeleteCategorie"].Value = "DELETE";
        }

        private void tsItemSave_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Shop));
            StreamWriter writer = new StreamWriter(Application.StartupPath + @"\shop.xml");
            serializer.Serialize(writer, this.coffeeShop);
            writer.Close();
        }

        private void tsItemSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "xml files (.xml)|*.xml";

            DialogResult rezultat = dialog.ShowDialog();
            if (rezultat == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Shop));
                StreamWriter writer = new StreamWriter(dialog.FileName);
                serializer.Serialize(writer, this.coffeeShop);
                writer.Close();
            }
        }

        private void tsItemOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "xml files (*.xml)|*.xml";

            DialogResult rezultat = dialog.ShowDialog();
            if (rezultat == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Shop));
                StreamReader reader = new StreamReader(dialog.FileName);
                this.coffeeShop = (Shop)serializer.Deserialize(reader);

                this.dgvCategories.Rows.Clear();
                for (int i = 0; i < this.coffeeShop.Categories.Count; i++)
                {
                    this.addCategoryInDGV(this.coffeeShop.Categories[i]);
                }
            }
        }

        private void tsItemNew_Click(object sender, EventArgs e)
        {
            this.coffeeShop = new Shop();
            this.dgvCategories.Rows.Clear();
        }
    }
}
