﻿using System.Collections.Generic;

namespace WindowsFormsApp1
{
    public class Shop
    {
        public List<Category> Categories { get; set; }
        public List<Product> Products { get; set; }

        public Shop()
        {
            this.Categories = new List<Category>();
            this.Products = new List<Product>();
        }
    }
}
