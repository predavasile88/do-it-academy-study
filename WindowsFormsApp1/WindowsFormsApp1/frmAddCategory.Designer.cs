﻿namespace WindowsFormsApp1
{
    partial class frmAddCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNume = new System.Windows.Forms.Label();
            this.txtNume = new System.Windows.Forms.TextBox();
            this.rbDa = new System.Windows.Forms.RadioButton();
            this.rbNu = new System.Windows.Forms.RadioButton();
            this.lblIsNew = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.txtImagine = new System.Windows.Forms.TextBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pbImagine = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagine)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNume
            // 
            this.lblNume.AutoSize = true;
            this.lblNume.Location = new System.Drawing.Point(2, 25);
            this.lblNume.Name = "lblNume";
            this.lblNume.Size = new System.Drawing.Size(38, 13);
            this.lblNume.TabIndex = 0;
            this.lblNume.Text = "Nume:";
            // 
            // txtNume
            // 
            this.txtNume.Location = new System.Drawing.Point(99, 22);
            this.txtNume.Name = "txtNume";
            this.txtNume.Size = new System.Drawing.Size(269, 20);
            this.txtNume.TabIndex = 1;
            // 
            // rbDa
            // 
            this.rbDa.AutoSize = true;
            this.rbDa.Location = new System.Drawing.Point(99, 67);
            this.rbDa.Name = "rbDa";
            this.rbDa.Size = new System.Drawing.Size(40, 17);
            this.rbDa.TabIndex = 2;
            this.rbDa.TabStop = true;
            this.rbDa.Text = "DA";
            this.rbDa.UseVisualStyleBackColor = true;
            // 
            // rbNu
            // 
            this.rbNu.AutoSize = true;
            this.rbNu.Location = new System.Drawing.Point(155, 67);
            this.rbNu.Name = "rbNu";
            this.rbNu.Size = new System.Drawing.Size(41, 17);
            this.rbNu.TabIndex = 3;
            this.rbNu.TabStop = true;
            this.rbNu.Text = "NU";
            this.rbNu.UseVisualStyleBackColor = true;
            // 
            // lblIsNew
            // 
            this.lblIsNew.AutoSize = true;
            this.lblIsNew.Location = new System.Drawing.Point(2, 69);
            this.lblIsNew.Name = "lblIsNew";
            this.lblIsNew.Size = new System.Drawing.Size(84, 13);
            this.lblIsNew.TabIndex = 4;
            this.lblIsNew.Text = "Categorie Noua:";
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Location = new System.Drawing.Point(5, 108);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(47, 13);
            this.lblImage.TabIndex = 5;
            this.lblImage.Text = "Imagine:";
            // 
            // txtImagine
            // 
            this.txtImagine.Enabled = false;
            this.txtImagine.Location = new System.Drawing.Point(99, 108);
            this.txtImagine.Name = "txtImagine";
            this.txtImagine.ReadOnly = true;
            this.txtImagine.Size = new System.Drawing.Size(269, 20);
            this.txtImagine.TabIndex = 6;
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(380, 106);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 7;
            this.btnPreview.Text = "...";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // pbImagine
            // 
            this.pbImagine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbImagine.Location = new System.Drawing.Point(96, 145);
            this.pbImagine.Name = "pbImagine";
            this.pbImagine.Size = new System.Drawing.Size(359, 231);
            this.pbImagine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagine.TabIndex = 8;
            this.pbImagine.TabStop = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(96, 406);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(156, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "ADAUGARE CATEGORIE";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(380, 406);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "ANULARE";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmAddCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.pbImagine);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.txtImagine);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lblIsNew);
            this.Controls.Add(this.rbNu);
            this.Controls.Add(this.rbDa);
            this.Controls.Add(this.txtNume);
            this.Controls.Add(this.lblNume);
            this.Name = "frmAddCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adaugare Categorie";
            ((System.ComponentModel.ISupportInitialize)(this.pbImagine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNume;
        private System.Windows.Forms.TextBox txtNume;
        private System.Windows.Forms.RadioButton rbDa;
        private System.Windows.Forms.RadioButton rbNu;
        private System.Windows.Forms.Label lblIsNew;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.TextBox txtImagine;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.PictureBox pbImagine;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
    }
}