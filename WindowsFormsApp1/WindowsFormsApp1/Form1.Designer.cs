﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsItemFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsItemNew = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsItemOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsItemEdit = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsItemAddCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsItemAbout = new System.Windows.Forms.ToolStripButton();
            this.dgvCategories = new System.Windows.Forms.DataGridView();
            this.colNumeCategorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIsNewCategorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetaliiCategorie = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colEditCategorie = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colDeleteCategorie = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tsItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsItemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsItemFile,
            this.tsItemEdit,
            this.tsItemAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(754, 25);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsItemFile
            // 
            this.tsItemFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsItemNew,
            this.toolStripSeparator1,
            this.tsItemOpen,
            this.toolStripSeparator2,
            this.tsItemSave,
            this.tsItemSaveAs,
            this.tsItemExit});
            this.tsItemFile.Image = ((System.Drawing.Image)(resources.GetObject("tsItemFile.Image")));
            this.tsItemFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsItemFile.Name = "tsItemFile";
            this.tsItemFile.Size = new System.Drawing.Size(41, 22);
            this.tsItemFile.Text = "FILE";
            // 
            // tsItemNew
            // 
            this.tsItemNew.Name = "tsItemNew";
            this.tsItemNew.Size = new System.Drawing.Size(180, 22);
            this.tsItemNew.Text = "New Shop";
            this.tsItemNew.Click += new System.EventHandler(this.tsItemNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // tsItemOpen
            // 
            this.tsItemOpen.Name = "tsItemOpen";
            this.tsItemOpen.Size = new System.Drawing.Size(180, 22);
            this.tsItemOpen.Text = "Open Shop";
            this.tsItemOpen.Click += new System.EventHandler(this.tsItemOpen_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // tsItemExit
            // 
            this.tsItemExit.Image = ((System.Drawing.Image)(resources.GetObject("tsItemExit.Image")));
            this.tsItemExit.Name = "tsItemExit";
            this.tsItemExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.tsItemExit.Size = new System.Drawing.Size(180, 22);
            this.tsItemExit.Text = "Exit";
            this.tsItemExit.Click += new System.EventHandler(this.tsItemExit_Click);
            // 
            // tsItemEdit
            // 
            this.tsItemEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsItemAddCategory});
            this.tsItemEdit.Image = ((System.Drawing.Image)(resources.GetObject("tsItemEdit.Image")));
            this.tsItemEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsItemEdit.Name = "tsItemEdit";
            this.tsItemEdit.Size = new System.Drawing.Size(44, 22);
            this.tsItemEdit.Text = "EDIT";
            // 
            // tsItemAddCategory
            // 
            this.tsItemAddCategory.Name = "tsItemAddCategory";
            this.tsItemAddCategory.Size = new System.Drawing.Size(147, 22);
            this.tsItemAddCategory.Text = "Add Category";
            this.tsItemAddCategory.Click += new System.EventHandler(this.tsItemAddCategory_Click);
            // 
            // tsItemAbout
            // 
            this.tsItemAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsItemAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsItemAbout.Image = ((System.Drawing.Image)(resources.GetObject("tsItemAbout.Image")));
            this.tsItemAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsItemAbout.Name = "tsItemAbout";
            this.tsItemAbout.Size = new System.Drawing.Size(50, 22);
            this.tsItemAbout.Text = "ABOUT";
            this.tsItemAbout.Click += new System.EventHandler(this.tsItemAbout_Click);
            // 
            // dgvCategories
            // 
            this.dgvCategories.AllowUserToAddRows = false;
            this.dgvCategories.AllowUserToDeleteRows = false;
            this.dgvCategories.AllowUserToResizeRows = false;
            this.dgvCategories.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.dgvCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNumeCategorie,
            this.colIsNewCategorie,
            this.colDetaliiCategorie,
            this.colEditCategorie,
            this.colDeleteCategorie});
            this.dgvCategories.Location = new System.Drawing.Point(0, 28);
            this.dgvCategories.MultiSelect = false;
            this.dgvCategories.Name = "dgvCategories";
            this.dgvCategories.RowHeadersVisible = false;
            this.dgvCategories.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCategories.Size = new System.Drawing.Size(556, 224);
            this.dgvCategories.TabIndex = 9;
            // 
            // colNumeCategorie
            // 
            this.colNumeCategorie.HeaderText = "Nume";
            this.colNumeCategorie.Name = "colNumeCategorie";
            this.colNumeCategorie.ReadOnly = true;
            // 
            // colIsNewCategorie
            // 
            this.colIsNewCategorie.HeaderText = "Este Noua";
            this.colIsNewCategorie.Name = "colIsNewCategorie";
            this.colIsNewCategorie.ReadOnly = true;
            // 
            // colDetaliiCategorie
            // 
            this.colDetaliiCategorie.HeaderText = "Detalii";
            this.colDetaliiCategorie.Name = "colDetaliiCategorie";
            // 
            // colEditCategorie
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Red;
            this.colEditCategorie.DefaultCellStyle = dataGridViewCellStyle5;
            this.colEditCategorie.HeaderText = "Editare";
            this.colEditCategorie.Name = "colEditCategorie";
            // 
            // colDeleteCategorie
            // 
            this.colDeleteCategorie.HeaderText = "Delete";
            this.colDeleteCategorie.Name = "colDeleteCategorie";
            // 
            // tsItemSave
            // 
            this.tsItemSave.Name = "tsItemSave";
            this.tsItemSave.Size = new System.Drawing.Size(180, 22);
            this.tsItemSave.Text = "Save Shop";
            this.tsItemSave.Click += new System.EventHandler(this.tsItemSave_Click);
            // 
            // tsItemSaveAs
            // 
            this.tsItemSaveAs.Name = "tsItemSaveAs";
            this.tsItemSaveAs.Size = new System.Drawing.Size(180, 22);
            this.tsItemSaveAs.Text = "Save Shop As...";
            this.tsItemSaveAs.Click += new System.EventHandler(this.tsItemSaveAs_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 450);
            this.Controls.Add(this.dgvCategories);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "dgfgh";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton tsItemFile;
        private System.Windows.Forms.ToolStripMenuItem tsItemNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsItemOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsItemExit;
        private System.Windows.Forms.ToolStripDropDownButton tsItemEdit;
        private System.Windows.Forms.ToolStripMenuItem tsItemAddCategory;
        private System.Windows.Forms.ToolStripButton tsItemAbout;
        private System.Windows.Forms.DataGridView dgvCategories;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumeCategorie;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsNewCategorie;
        private System.Windows.Forms.DataGridViewButtonColumn colDetaliiCategorie;
        private System.Windows.Forms.DataGridViewButtonColumn colEditCategorie;
        private System.Windows.Forms.DataGridViewButtonColumn colDeleteCategorie;
        private System.Windows.Forms.ToolStripMenuItem tsItemSave;
        private System.Windows.Forms.ToolStripMenuItem tsItemSaveAs;
    }
}

